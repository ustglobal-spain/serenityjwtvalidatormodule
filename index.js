'use strict';

var appRoot = require('app-root-path');
var config = require(appRoot + '/configuration.js').config;
var Log = require('log'),
  log = new Log(config.logLevel); // debug, info, error
var jwt = require('jsonwebtoken');
var request = require('requestretry');

var header = "-----BEGIN PUBLIC KEY-----\n";
var end = "\n-----END PUBLIC KEY-----";
var enter = "\n";
var publicKey = config.publicKeyProvider.defaultKey;


function providedUrl(){
  if(config.publicKeyProvider.url){
    return true;
  }
  else{
    return false;
  }
}

function myDelayStrategy(err, response, body){
  return Math.floor(config.publicKeyProvider.delayBetweenAttemps);
}

var getPublicKey = function(){
  var req = config.publicKeyProvider.url + '/' + config.publicKeyProvider.keyIdentifier;
  request.get({
    url: req,
    maxAttempts: config.publicKeyProvider.maxAttemps,
    delayStrategy: myDelayStrategy
  }, function(err, response, body){
    if (!err && response.statusCode == 200) {
      log.info('Got the public key');
      if (typeof(body) === 'string'){
        var bodyParse = JSON.parse(body);
        publicKey = bodyParse.key;
      }
      else {
        publicKey = body.key;
      }
    }
    else{
      log.error('Maximum number of attempts exceeded, using a default publicKey');
    }
  });
}

var insertEnters = function(input){
  var cont = 1;
  var output = '';
  for(var i=0; i<input.length; i++){
    output = output + input.charAt(i);
    if(cont == 64){
      output = output + enter;
      cont = 0;
    }
    cont = cont +1;
  }
  output = header + output + end;
  return output;
}

var verify = function(token){
  try{
    var pubKeyParsed = insertEnters(publicKey);
    var decoded = jwt.verify(token, pubKeyParsed, { algorithms: ['RS256'] });
    return true;
  }
  catch(err){
    return false;
  }
}


var validateToken = function(req, res, next){
  //var token = 'eyJraWQiOiJJRFBTRVJFTklUWV9TSEExd2l0aFJTQSIsInR5cCI6IkpXVCIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiJ4MDIxMDk2IiwiYXVkIjoiU0FSQVNFUkVOSVRZIiwibmJmIjoxNDczNzU5ODk0LCJpc3MiOiJJRFBTRVJFTklUWSIsImV4cCI6MzYwMTQ3Mzc1OTg5NCwiaWF0IjoxNDczNzU5ODk0LCJqdGkiOiI5MDgxMTk0NS1jMjIyLTRkOWQtYWY5NS04MjAwOTRiMDg3ZmYifQ.a5hNefm9KuEfp-cjt9_-p6tSeMEVMvHVHAry175eV_dOVloPB0gKnlh4x7GbTc4egpstX0JNZzMN5pojLHj4efW-1fREIWpzyGuup4KXoqLXcJxihx22x1-Fmru2dkeY2tPVIwI99sCIn_RdwyPhDALK6WsRO3OOe8_MRSitTYfObaQLLQ-QMyrh8Yygc9FQvcxKOgH7S8dwrU4IcnUOxJAW7tCq_0xZtBd_HRfEzhCIytIfcSxw1gvRNbuxgx5ZDgwOvqgJCS4MyK6ye1Di6ZITvNq5oYNJ5aA5qyohqJ_M-Q1INaBnVO5qUIotjG0m_9WH1OcCVJr_U_86aCoDCg';
  //Current name of the field
  var token = req.headers['gs-auth-token'];
  //To future name of the field
  //var token = req.headers.authorization;
  if(verify(token)){
    next();
  }
  else{
    var notFound = {
      code: '405',
      message: 'Error verification token'
    }
    res.status(405).send(notFound);
  }
}

module.exports = {
  functions:{
    getPublicKey: getPublicKey,
    providedUrl: providedUrl,
    validateToken: validateToken
  }
};
