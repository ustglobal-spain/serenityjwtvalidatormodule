# Description

Nodejs module to validate **Serenity Token JWT**

- It is used by fundSenderAPI

It receives a JWT Token from a Serenity application and using a public key from a key server,
it checks the token and allow or not to continue with the operation.

## Dependencies
- This module needs a configuration file in main application directory: `configuration.json`

# Managment token JWT with nodejs applications

## Requeriments

 - Express 4.x
 - Jsonwebtoken

## Getting started

Using Express 4.x we can handle request made to our API, to perform validation and verification of JWT tokens.
The necessary function to handle requests is app.all(path, callback [, callback…]), where app is a variable that refers to the Express library.
In particular this function is responsible for handling all requests than fit the path defined in the function itself. In the variable path, we can indicate regular expression to adjust this behavior. For example:

- App.all ( ‘ * ’ , function ) , in this case the  function will apply to all requests of our API.
- App.all ( ‘ /api/* ’ , function ) , in this case the function will apply to request whose path matches the pattern defined in the variable, for example:
 - A request like /api/get/{id}: will be handle in this case.
 - A request like /noapi/get/{id}: won’t be handle in this case.

The function must be a callback function, must have the parameters request, response and next.
In this case, this callback function will be used to validate and verify the Token JWT of all API requests. The advantage is not modify the APIS one by one, only modifying the definition of the application will be enough.
The next step is to validate and verify the token JWT provided for the API Manager, we will use for this task Jsonwebtoken nodejs library.
This library will allow us to easily verify the token provided for the API Manager. First we get a public key making a request to a server of keys. If this server fails, a limited number of times defined by the programmer will be retried.
Once we have the public key, we can use the function ‘verify’ of the library to verify the signature of the token JWT.
Before using the verify function, we must make a parsing to transform the format of the public key (string) to a PEM format to use RSA algorithms. Having verified the signature token, we can use the function ‘decode’ of the library to extract the payload of the token.



## Using tokenValidator library

 - add to file the application package.json the url of gitlab where is the library. https://serenity-gitlab.alm.gsnetcloud.corp/blockchain/JWTTokenValidator.
 - Add to the application in nodejs the function ‘app.all’, and pass as a parameter the function ‘validateToken’ of the library. With this we get all our API calls made nodejs apply the function to validate token.
 - add a configuration file where the endpoint is defined and the key to get the public key.

## Scope of the function validateToken
This function is responsible for verifying that the token JWT is signed by the Gateway.

## Getting the public key to verify the token JWT
A query is made to the security service for the public key necessary for verification of JWT.
